#!/bin/bash
#
# the first 100, anyways
#
curl -s -H "Authorization: Bearer $(curl -s --json '{"identifier": "aliceisjustplaying.bsky.social", "password": "letmein"}' https://bsky.social/xrpc/com.atproto.server.createSession | jq -j ".accessJwt")" "https://bsky.social/xrpc/app.bsky.graph.getMutes?limit=100" | jq -r '.mutes | .[] | .did' | xargs -I{} -P10 curl -s 'https://plc.directory/{}' | jq -r '.alsoKnownAs[0]' | sed -e 's#at://#@#' | sort